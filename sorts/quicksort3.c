#include <inttypes.h>

#include "sorts.h"

void swapQS3(int64_t *a, int64_t *b)
{
    int64_t tmp = *a;
    *a = *b;
    *b = tmp;
}

void partitionQS3(int64_t data[], int64_t l, int64_t r, int64_t *i, int64_t *j)
{
    *i = l - 1;
    *j = r;
    int64_t p = l - 1;
    int64_t q = r;
    int64_t v = data[r];

    while (1)
    {
        while (data[++(*i)] < v);

        while (v < data[--(*j)])
            if (*j == 1)
                break;

        if (*i >= *j) break;

        swapQS3(&data[*i], &data[*j]);

        if (data[*i] == v)
        {
            p++;
            swapQS3(&data[p], &data[*i]);
        }

        if (data[*j] == v)
        {
            q--;
            swapQS3(&data[*j], &data[q]);
        }
    }

    swapQS3(&data[*i], &data[r]);

    *j = *i - 1;
    for (int64_t k = l; k < p; k++, (*j)--)
        swapQS3(&data[k], &data[*j]);

    (*i)++;
    for (int64_t k = r - 1; k > q; k--, (*i)++)
        swapQS3(&data[*i], &data[k]);
}

void quickSort3(int64_t data[], int64_t last, int64_t first)
{
    if (last <= first) return;

    int64_t i, j;

    partitionQS3(data, first, last, &i, &j);

    quickSort3(data, j, first);
    quickSort3(data, last, i);
}
