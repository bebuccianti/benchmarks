#include <stdlib.h>

#include "sorts.h"

void merge(int64_t arr[], int64_t l, int64_t m, int64_t r) 
{ 
    int64_t i, j, k; 
    int64_t n1 = m - l + 1; 
    int64_t n2 =  r - m; 
  
    /* create temp arrays */
    int64_t *L = (int64_t*)malloc(n1*sizeof(int64_t));
    int64_t *R = (int64_t*)malloc(n2*sizeof(int64_t));
  
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) 
        *(L+i) = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        *(R+j) = arr[m + 1+ j]; 
  
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = l; // Initial index of merged subarray 
    while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    }

    free(L);
    free(R);
} 
  
/* l is for left index and r is right index of the 
   sub-array of arr to be sorted */
void mergeSort(int64_t arr[], int64_t r, int64_t l)
{
    if (l < r) 
    { 
        // Same as (l+r)/2, but avoids overflow for 
        // large l and h 
        int64_t m = l+(r-l)/2; 
  
        // Sort first and second halves 
        mergeSort(arr, m, l); 
        mergeSort(arr, r, m+1); 
  
        merge(arr, l, m, r); 
    } 
}
