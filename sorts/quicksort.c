#include <inttypes.h>

#include "sorts.h"

void swapQS(int64_t *a, int64_t *b)
{
    int64_t tmp = *a;
    *a = *b;
    *b = tmp;
}

int64_t partitionQS(int64_t data[], int64_t first, int64_t last)
{
    int64_t pivot = data[last];
    int64_t i = (first - 1);

    for (int64_t j = first; j <= last - 1; j++)
    {
        if (data[j] <= pivot)
        {
            i++;
            swapQS(&data[i], &data[j]);
        }
    }
    swapQS(&data[i+1], &data[last]);
    return (i + 1);
}

void quickSort(int64_t data[], int64_t last, int64_t first)
{
    if (first < last)
    {
        int64_t q = partitionQS(data, first, last);
        quickSort(data, q-1, first);
        quickSort(data, last, q+1);
    }
}
