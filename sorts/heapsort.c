#include <stdio.h>

#include "sorts.h"

void swapHS(int64_t *x, int64_t *y)
{
    int64_t tmp = *x;
    *x = *y;
    *y = tmp;
}

void heapify(int64_t arr[], int64_t size, int64_t i)
{
    int64_t largest = i;
    int64_t l = 2*i + 1;
    int64_t r = 2*i + 2;

    if (l < size && arr[l] > arr[largest])
        largest = l;

    if (r < size && arr[r] > arr[largest])
        largest = r;

    if (largest != i)
    {
        swapHS(&arr[i], &arr[largest]);
        heapify(arr, size, largest);
    }
}

void heapSort(int64_t data[], int64_t size, int64_t trash)
{
    int64_t i;
    for (i = (size / 2) - 1; i >= 0; i--)
        heapify(data, size, i);

    for (i = size - 1; i >= 0; i--)
    {
        swapHS(&data[0], &data[i]);
        heapify(data, i, 0);
    }
}
