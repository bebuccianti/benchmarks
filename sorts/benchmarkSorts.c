#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include <string.h>

#include "sorts.h"

int comp(const void *x, const void *y)
{
    int f = *((int*)x);
    int s = *((int*)y);
    if (f > s) return 1;
    if (f < s) return -1;
    return 0;
}

double runTests(int64_t n, void (*fn)())
{
    int64_t i;

    srand(time(NULL));
    int64_t *array = (int64_t*)malloc(n*sizeof(int64_t));

    for (i = 0; i < n; i++)
        *(array+i) = rand();

    clock_t start = clock();

    if (fn == &mergeSort || fn == &quickSort3)
        fn(array, n, 0);
    else
        fn(array, n, 1);
        
    clock_t end = clock();

    free(array);
        
    return (double)(end-start)/CLOCKS_PER_SEC;
}

int main()
{
    int64_t i;
    const char header[] = "size\t\tquicksort\theapsort\tmergesort\tquicksort3";
    
    printf("%s\n", header);

    for (i = 8; i < 17000000; i*=2)
    {
        printf("%8ld\t%fs\t%fs\t%fs\t%fs\n",
               i,
               runTests(i, &quickSort),
               runTests(i, &heapSort),
               runTests(i, &mergeSort),
               runTests(i, &quickSort3));
    }

    return 0;
}
